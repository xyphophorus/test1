package hello;

import org.springframework.ui.ExtendedModelMap;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

// This is the unit tests file.

public class GreetingControllerTest {
    
    private hello.GreetingController cot;   // "Code-of-test"
    
    // An arbitrary concrete class which inherits from ui.Model, which is what 
    private org.springframework.ui.ExtendedModelMap model;
    
    @BeforeMethod()
    protected void setUp() {
        cot = new hello.GreetingController();
        model = new org.springframework.ui.ExtendedModelMap();
        System.out.println("EGoodrich DEBUG: unit setUp test method has run.");
    }
    
    @Test()
    public void greetingTestNameValueSet() {
        System.out.println("EGoodrich DEBUG: unit greetingTestNameValueSet test method is starting.");
        for (String testName : new String[] {null, "a", "A", "World", "Fred", "Frederick Flintstone"}) {
            
            String result = cot.greeting(testName, model);
            Assert.assertEquals("greeting", result, "- CoT method always returns static string \"greeting\". -");
            Assert.assertTrue(model.containsAttribute("name"), "- CoT method should set \"name\" attribute.");
            Assert.assertEquals(testName, model.asMap().get("name"),
                "- CoT method should set \"name\" attribute to input \"name\" value. -");
        }
        System.out.println("EGoodrich DEBUG: unit greetingTestNameValueSet test method is ending.");
    }
    
    @Test()
    public void goodbyeTestNameValueSet() {
        System.out.println("EGoodrich DEBUG: unit goodbyeTestNameValueSet test method is starting.");
        for (String testName : new String[] {"World"/*, "John Q. Public"*/, "Eugene"}) {
            
            String result = cot.goodbye(testName, model);
            Assert.assertEquals("goodbye", result, "- CoT method always returns static string \"goodbye\". -");
            Assert.assertTrue(model.containsAttribute("name"), "- CoT method should set \"name\" attribute.");
            Assert.assertEquals(testName, model.asMap().get("name"),
                "- CoT method should set \"name\" attribute to input \"name\" value. -");
        }
        System.out.println("EGoodrich DEBUG: unit goodbyeTestNameValueSet test method is ending.");
    }
    
    @Test(enabled=false)
    public void generalTestAlwaysFails() {
        System.out.println("EGoodrich DEBUG: unit generalTestAlwaysFails test method is starting.");
        Assert.assertEquals("Alpha", "Bravo", "- purposeful always fail for CD pipeline testing purposes. -");
        System.out.println("EGoodrich DEBUG: unit generalTestAlwaysFails test method is ending.");
    }
    
    @Test(enabled=false)
    public void generalTestFailsOneOutOfTwo() {
        System.out.println("EGoodrich DEBUG: unit generalTestFailsOneOutOfTwo test method is starting.");
        Assert.assertTrue((System.currentTimeMillis() % 2) > 0, "- mod 2 purposeful fail for CD pipeline testing purposes. -");
        System.out.println("EGoodrich DEBUG: unit generalTestFailsOneOutOfTwo test method is ending.");
    }
    
    @Test(enabled=false)
    public void generalTestFailsOneOutOfFive() {
        System.out.println("EGoodrich DEBUG: unit generalTestFailsOneOutOfFive test method is starting.");
        Assert.assertTrue((System.currentTimeMillis() % 5) > 0, "- mod 5 purposeful fail for CD pipeline testing purposes. -");
        System.out.println("EGoodrich DEBUG: unit generalTestFailsOneOutOfFive test method is ending.");
    }
}