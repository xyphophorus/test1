package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
    
    @RequestMapping("/goodbye")
    public String goodbye(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        /*if (name == "Eugene" || name == "John Q. Public") {
            model.addAttribute("commentary", "I always like when you visit.");
        }*/
        
        /*
        if (name == "Eugene")
        {
            model.addAttribute("commentary", "I always like when you visit.");
        }
        else if (name == "John Q. Public")
        {
            model.addAttribute("commentary", "I always like when you visit.");
        }
        else*/ if (name != "World") {
            model.addAttribute("commentary", "It was nice of you to visit.");
        }

            
        return "goodbye";
    }

}
