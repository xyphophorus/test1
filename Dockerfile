FROM egoodrich_alp32_jdk8:v1
# Last edited 2015-12-07 by EGoodrich
# TODO: in the future we may want this to be wildcard.
# Being explicit for now so we know exactly what we're copying, and where.
COPY build/libs/helloworld-service-0.1.0.jar /opt/hello_world/
LABEL hello-world.component="service" \
      hello-world.git-branch="!GIT_BRANCH!" \
      hello-world.git-commit="!GIT_COMMIT!" \
      hello-world.build-number="!BUILD_NUMBER!" \
      hello-world.build-tag="!BUILD_TAG!" \
      hello-world.build-date-utc="!DATE_UTC!"
CMD java -jar /opt/hello_world/gs-serving-web-content-0.1.0.jar