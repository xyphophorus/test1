package helloWorldIntegrationTest;

import com.beust.jcommander.*;
import java.util.*;
import org.testng.TestNG;
import org.testng.xml.*;

class IntegrationTestMain {

    public static void main(String[] args) {
        // From https://stackoverflow.com/questions/16465695/how-to-run-testng-tests-from-main-in-an-executable-jar
        CommandLineOptions options = new CommandLineOptions();
        JCommander jCommander = new JCommander(options, args);
        XmlSuite suite = new XmlSuite();
        suite.setName("TestSuite1");
        suite.setParameters(options.convertToMap());

        List<XmlClass> classes = new ArrayList<XmlClass>();
        classes.add(new XmlClass("helloRestClient.GreetingRestServiceTest"));

        XmlTest test = new XmlTest(suite);
        test.setName("helloRestClientTests");
        test.setXmlClasses(classes);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

        TestNG testNG = new TestNG();
        testNG.setXmlSuites(suites);
        testNG.run();
    }
}
