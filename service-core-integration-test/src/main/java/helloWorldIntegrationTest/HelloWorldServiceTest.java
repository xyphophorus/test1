package helloWorldRestClient;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

// This is the simulated Integration tests file.

public class HelloWorldServiceTest {
    
    @Test()
    public void printIntegrationTestRan() {
        System.out.println("EGoodrich DEBUG: printIntegrationTestRan test method has run.");
    }
    
    @Test(enabled=false)
    public void generalTestAlwaysFails() {
        System.out.println("EGoodrich DEBUG: generalTestAlwaysFails test method is starting.");
        Assert.assertEquals("Alpha", "Bravo", "- purposeful always fail for CD pipeline testing purposes. -");
        System.out.println("EGoodrich DEBUG: Integration generalTestAlwaysFails test method is ending.");
    }
    
    @Test(enabled=false)
    public void generalTestFailsOneOutOfFive() {
        System.out.println("EGoodrich DEBUG: generalTestFailsOneOutOfFive test method is starting.");
        Assert.assertTrue((System.currentTimeMillis() % 5) > 0, "- mod 5 purposeful fail for CD pipeline testing purposes. -");
        System.out.println("EGoodrich DEBUG: Integration generalTestFailsOneOutOfFive test method is ending.");
    }
}